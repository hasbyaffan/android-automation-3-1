# android-automation-3-1



## Definisi UI Map/Object Repository

UI Map adalah kumpulan lokator atau identifikator yang digunakan untuk menemukan dan berinteraksi dengan elemen-elemen interface pengguna (UI) dalam sebuah aplikasi. Lokator ini bisa berupa XPath, ID, CSS Selectors, dan metode identifikasi lainnya. Sementara itu, Object Repository adalah tempat penyimpanan terpusat dari semua elemen UI ini, umumnya digunakan dalam tools automation testing seperti Selenium atau QTP. Konsep ini memudahkan pengelolaan elemen UI, terutama saat terjadi perubahan pada interface aplikasi. Dengan menggunakan UI Map atau Object Repository, developer dan tester dapat dengan mudah memperbarui lokasi elemen di satu tempat, mengurangi redundansi, memperbaiki kolaborasi tim, menjaga konsistensi, dan meningkatkan skalabilitas dalam testing. Hal ini sangat penting untuk menjaga efisiensi dan efektivitas proses testing, terutama dalam Project yang besar dan kompleks.

## Kenapa Kita Membutuhkannya
### 1. Maintenance yang Lebih Mudah

Dengan adanya UI Map atau Object Repository, pemeliharaan skrip pengujian menjadi lebih mudah. Jika terdapat perubahan pada elemen UI, pengembang cukup memperbarui lokator di satu tempat saja tanpa harus mengubah seluruh skrip pengujian.

### 2. Pengurangan Redundansi

Menghindari duplikasi lokator elemen di banyak tempat. Hal ini meningkatkan efisiensi dan mengurangi kesalahan.

### 3. Kemudahan Dalam Kolaborasi

Ketika bekerja dalam tim, Object Repository memudahkan berbagi dan mengelola elemen UI yang digunakan oleh semua anggota tim.

### 4. Konsistensi

Menjaga konsistensi dalam mengidentifikasi elemen UI di seluruh skrip pengujian.

### 5. Skalabilitas

Memudahkan dalam mengelola elemen UI ketika aplikasi berkembang dan menjadi lebih kompleks.

