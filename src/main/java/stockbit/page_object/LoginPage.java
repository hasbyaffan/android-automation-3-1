package stockbit.page_object;


public class LoginPage extends BasePage {

    public void isOnboardingPage(){
        assertIsDisplay("IS_DISPLAY_ONBOARDING");
    }
    public void tapLogin(){

        tap("BUTTON_LOGIN_ENTRYPOINT");

    }


    public void inputUsername(String username){
        typeOn("TYPE_ON_USERNAME", username);

    }

    public void inputPassword(String password){
        typeOn("TYPE_ON_PASSWORD", password);

    }

    public void tapLoginButton(){

        tap("BUTTON_LOGIN");

    }

    public void tapSkipBiometric(){

        tap("BUTTON_SKIP_BIOMETRIC");
    }

    public void tapSkipAvatar(){

        tap("BUTTON_SKIP_AVATAR");
    }

    public void isWatchlistPage(){
        tapSkipBiometric();
        tapSkipAvatar();
        assertIsDisplay("IS_DISPLAY_WATCHLIST");
    }


}
