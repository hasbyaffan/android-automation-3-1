package stockbit.page_object;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import stockbit.android_driver.AndroidDriverInstance;
import stockbit.utils.Utils;

import static stockbit.utils.Utils.printError;

public class BasePage {
    public AndroidDriver driver(){
        return AndroidDriverInstance.androidDriver;
    }

    public By element(String elementLocator){
        String elementValue = Utils.ELEMENTS.getProperty(elementLocator);

        if (elementValue == null){
            printError("Element " + elementLocator + "not found! Please Check Your Properties File!");
            throw new NoSuchElementException("Couldn't find element: " + elementLocator);
        } else {
            String[] locator = elementValue.split("_");
            String locatorType = locator[0];
            String locatorValue = elementValue.substring(elementValue.indexOf("_") + 1);

            return switch (locatorType) {
                case "id" -> By.id(locatorValue);
                case "xpath" -> By.xpath(locatorValue);
                default -> throw new IllegalStateException("Unexpected value: " + locatorType);
            };
        }
    }



    public void tap(String element){

        driver().findElement(element(element)).click();
    }

    public void typeOn(String element, String text){

        driver().findElement(element(element)).sendKeys(text);
    }

    public boolean isDisplayed(String element){

        return driver().findElement(element(element)).isDisplayed();
    }
    public void assertIsDisplay(String element){
        if (!isDisplayed(element)){
            throw new AssertionError(String.format("This element '%s' not found", element));
        }
    }
}
